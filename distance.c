//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
int input()
{
float p;
printf("Enter 'x' and 'y'  co ordinates of  2 points respectively:");
scanf("%f",&p);
return p;
}
int compute(float x1,float y1,float y2,float x2)
{
float s;
s=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
return s;

}
void dis(float x1,float y1,float y2,float x2,float s)
{
printf("Distance between %f %f  and %f %f is %f ",x1,y1,x2,y2,s);
}
void main()
{
float x1,y1,x2,y2,c;
x1=input();
y1=input();
x2=input();
y2=input();
c=compute(x1,y1,y2,x2);
dis(x1,y1,y2,x2,c);
}
